#!/usr/bin/python

import re

RX = re.compile('^Step ([A-Z]) must be finished before step ([A-Z]) can begin.$')


def browse(instrs, visited, to_visit, prov, deps):
    if len(to_visit) == 0:
        return visited
    cur_node = sorted(to_visit)[0]
    to_visit.remove(cur_node)
    visited.append(cur_node)
    if cur_node in prov:
        next_nodes = [c for c in prov[cur_node] if all(k in visited for k in deps[c]) and c not in visited]
        to_visit.extend(next_nodes)
    return browse(instrs, visited, to_visit, prov, deps)


def calc(filename):
    with open(filename, 'r') as of:
        instrs = list(map(lambda s: RX.match(s).groups(), of.readlines()))
        left = set([i[0] for i in instrs])
        right = set([i[1] for i in instrs])
        nodes = left.union(right)
        prov = {k: [i[1] for i in instrs if i[0] == k] for k in left}
        deps = {v: [i[0] for i in instrs if i[1] == v] for v in right}
        to_visit = left.difference(right)
        first = sorted(list(to_visit))[0]
        to_visit.remove(first)
        to_visit = list(to_visit)
        to_visit.extend([p for p in prov[first] if len(deps[p]) == 1])
        result = browse(instrs, [first], to_visit, prov, deps)
        return ''.join(result)


def dowork(workers, done, todo, offset, deps):
    doable = [t for t in todo if t not in done and (t not in deps or all([d in done for d in deps[t]]))]
    d = sorted(doable)[-1]
    rt = ord(d) - 64 + offset
    possible_start = 0
    if d in deps:
        possible_start = max([done[td] for td in deps[d]])
    potential_finish = {w: max(tw, possible_start) + rt for w, tw in workers.items()}
    assignee = min(potential_finish, key=potential_finish.get)
    start_task_at = max(workers[assignee], possible_start)
    workers[assignee] = start_task_at + rt
    done.update({d: workers[assignee]})
    todo.remove(d)
    if len(todo) == 0:
        return workers
    return dowork(workers, done, todo, offset, deps)


def calc2(filename, nbworkers, offset):
    with open(filename, 'r') as of:
        instrs = list(map(lambda s: RX.match(s).groups(), of.readlines()))
        left = set([i[0] for i in instrs])
        right = set([i[1] for i in instrs])
        nodes = left.union(right)
        deps = {v: [i[0] for i in instrs if i[1] == v] for v in right}
        workers = {k: 0 for k in range(nbworkers)}
        result = dowork(workers, {}, list(nodes), offset, deps)
        return max(result.values())

if __name__ == '__main__':
    assert calc('test') == 'CABDFE'
    print(calc('input'))
    assert calc2('test', 2, 0) == 15
    print(calc2('input', 5, 60))
