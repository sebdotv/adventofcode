#!/usr/bin/python

import re

OFFSET = 4
INIT = re.compile(r'^initial state: ([#\.]+)$')
RULE = re.compile(r'^([#\.]{5}) => ([#\.])$')


def read_input(filename):
    with open(filename, 'r') as of:
        all_lines = list(map(str.strip, of.readlines()))
        init_state = INIT.match(all_lines[0]).group(1)
        rules = {k: v for k, v in list(map(lambda s: RULE.match(s).groups(), all_lines[2:]))}
        return init_state, rules


def grow(state, rules, idx):
    # Stable after 125 iterations, always increases by 88 afterwards
    state = '.' * (OFFSET - state.index('#')) + state + '.' * (OFFSET - state[::-1].index('#'))
    result = ''
    result = ''.join([rules[state[t:t+5]] for t in range(len(state) - 5)])
    idx -= OFFSET - 2 - result.index('#')
    return result, idx


def calc(filename, nb):
    state, rules = read_input(filename)
    idx = 0
    for i in range(nb):
        state, idx = grow(state, rules, idx)
    state = re.sub('\.*$', '', re.sub('^\.*', '', state))
    return sum([(k+idx) for k,v in enumerate(state) if v == '#'])


def part2(filename):
    return calc(filename, 125) + (50000000000 - 125) * 88


if __name__ == '__main__':
    print(calc('input', 20))
    print(part2('input'))
