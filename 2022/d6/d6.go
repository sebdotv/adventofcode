package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func calc(line string, length int) int {
	for i := length; i <= len(line); i++ {
		chunk := line[i-length : i]
		letters := make(map[int32]struct{})
		good := true
		for _, l := range chunk {
			_, already := letters[l]
			if already {
				good = false
				break
			}
			letters[l] = struct{}{}
		}
		if good {
			return i
		}
	}
	panic(fmt.Errorf("didn't find marker in %s", line))
}

func main() {
	expt1 := []int{7, 5, 6, 10, 11}
	expt2 := []int{19, 23, 23, 29, 26}
	for idx, line := range aoc.ReadInputLines("test.txt") {
		aoc.CheckEquals(expt1[idx], calc(line, 4))
		aoc.CheckEquals(expt2[idx], calc(line, 14))
	}
	inp := aoc.ReadInputLines("input.txt")[0]
	fmt.Println(calc(inp, 4))
	fmt.Println(calc(inp, 14))
}
