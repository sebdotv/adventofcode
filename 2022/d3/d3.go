package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	offsetLow = int("a"[0])
	offsetUp  = int("A"[0])
)

func priority(side string) (result *aoc.Set[int]) {
	result = aoc.NewSet[int]()
	for _, item := range side {
		if v := int(item); v >= offsetLow {
			result.Add(v - offsetLow + 1)
		} else {
			result.Add(v - offsetUp + 27)
		}
	}
	return
}

func calc(filename string) int {
	rucksack := aoc.ReadInputLines(filename)
	result := 0
	for _, line := range rucksack {
		inter := priority(line[:len(line)/2]).Intersection(priority(line[len(line)/2:]))
		result += aoc.Sum(inter.ToList())
	}
	return result
}

func calc2(filename string) int {
	allElves := aoc.ReadInputLines(filename)
	result := 0
	for groupId := 0; 3*groupId < len(allElves); groupId++ {
		items := priority(allElves[3*groupId]).Intersection(priority(allElves[3*groupId+1])).Intersection(priority(allElves[3*groupId+2]))
		result += aoc.Sum(items.ToList())
	}
	return result
}

func main() {
	aoc.CheckEquals(157, calc("test.txt"))
	aoc.CheckEquals(70, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
