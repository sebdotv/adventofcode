package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"golang.org/x/exp/slices"
	"strconv"
	"strings"
)

type monkey struct {
	operation func(int64) int64
	test      func(int64) bool
	items     []int64
	destTrue  int
	destFalse int
}

func guessOperation(funcDef string) func(int64) int64 {
	opChunks := strings.Split(funcDef, " ")
	op := opChunks[4]
	right := opChunks[5]
	if op == "+" {
		rightInt, _ := strconv.ParseInt(right, 10, 64)
		return func(a int64) int64 {
			return a + rightInt
		}
	} else {
		if right == "old" {
			return func(a int64) int64 {
				return a * a
			}
		} else {
			rightInt, _ := strconv.ParseInt(right, 10, 64)
			return func(a int64) int64 {
				return a * rightInt
			}
		}
	}
}

func guessTest(testDef string) (func(int64) bool, int64) {
	testChunks := strings.Split(testDef, " ")
	div, _ := strconv.ParseInt(testChunks[3], 10, 64)
	return func(a int64) bool {
		return a%div == 0
	}, div
}

func parseMonkey(def []string) (*monkey, int64) {
	destTrue, _ := strconv.Atoi(strings.Split(def[4], " ")[5])
	destFalse, _ := strconv.Atoi(strings.Split(def[5], " ")[5])
	itemsChunks := strings.Split(strings.Replace(def[1], ",", "", -1), " ")[2:]
	items := make([]int64, len(itemsChunks))
	for i, itemStr := range itemsChunks {
		items[i], _ = strconv.ParseInt(itemStr, 10, 64)
	}
	testFunc, div := guessTest(def[3])
	return &monkey{
		items:     items,
		operation: guessOperation(def[2]),
		test:      testFunc,
		destTrue:  destTrue,
		destFalse: destFalse,
	}, div
}

func (m *monkey) inspectAndPass(allMonkeys []*monkey, relief bool, ppcm int64) int64 {
	for _, item := range m.items {
		item = m.operation(item) % ppcm
		if relief {
			item /= 3
		}
		if m.test(item) {
			allMonkeys[m.destTrue].receive(item)
		} else {
			allMonkeys[m.destFalse].receive(item)
		}
	}
	nbInspections := len(m.items)
	m.items = []int64{}
	return int64(nbInspections)
}

func (m *monkey) receive(item int64) {
	m.items = append(m.items, item)
}

func readInput(filename string) ([]*monkey, int64) {
	blocks := aoc.ReadByBlocks(filename)
	monkeys := make([]*monkey, len(blocks))
	var ppcm int64 = 1
	var div int64
	for i, block := range blocks {
		monkeys[i], div = parseMonkey(block)
		ppcm *= div
	}
	return monkeys, ppcm
}

func calc(filename string, nbIt int, relief bool) int64 {
	monkeys, ppcm := readInput(filename)
	inspections := make([]int64, len(monkeys))
	for round := 0; round < nbIt; round++ {
		for idx, mk := range monkeys {
			inspections[idx] += mk.inspectAndPass(monkeys, relief, ppcm)
		}
	}
	slices.Sort(inspections)
	return inspections[len(inspections)-1] * inspections[len(inspections)-2]
}

func main() {
	aoc.CheckEquals(10605, calc("test.txt", 20, true))
	aoc.CheckEquals(2713310158, calc("test.txt", 10000, false))
	fmt.Println(calc("input.txt", 20, true))
	fmt.Println(calc("input.txt", 10000, false))
}
