package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"sort"
)

func computeSums(filename string) []int {
	values := aoc.ReadIntsByBlocks(filename)
	sums := make([]int, len(values))
	for i, b := range values {
		for _, v := range b {
			sums[i] += v
		}
	}
	return sums
}

func calc(filename string) int {
	sums := computeSums(filename)
	return aoc.ListMax(sums)
}

func calc2(filename string) int {
	sums := computeSums(filename)
	sort.Ints(sums)
	topThree := sums[len(sums)-3:]
	return aoc.Sum(topThree)
}

func main() {
	aoc.CheckEquals(24000, calc("test.txt"))
	aoc.CheckEquals(45000, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
