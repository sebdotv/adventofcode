package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCompareInts(t *testing.T) {
	assert.Equal(t, 0, compareInts(5, 5))
	assert.Equal(t, -1, compareInts(5, 7))
	assert.Equal(t, 1, compareInts(5, 3))
}

func TestCompareIntToSlice(t *testing.T) {
	assert.Equal(t, 0, compareSlices([]interface{}{5}, []interface{}{5}))
	assert.Equal(t, -1, compareSlices([]interface{}{5}, []interface{}{6}))
	assert.Equal(t, -1, compareSlices([]interface{}{5}, []interface{}{5, 0}))
	assert.Equal(t, 1, compareSlices([]interface{}{5}, []interface{}{4}))
	assert.Equal(t, 1, compareSlices([]interface{}{5}, []interface{}{}))
}

func TestCompareSliceToInt(t *testing.T) {
	assert.Equal(t, 0, compareSlices([]interface{}{5}, []interface{}{5}))
	assert.Equal(t, 1, compareSlices([]interface{}{6}, []interface{}{5}))
	assert.Equal(t, 1, compareSlices([]interface{}{5, 0}, []interface{}{5}))
	assert.Equal(t, -1, compareSlices([]interface{}{4}, []interface{}{5}))
	assert.Equal(t, -1, compareSlices([]interface{}{}, []interface{}{5}))
}

func TestCompareSlices(t *testing.T) {
	assert.Equal(t, 0, compareSlices([]interface{}{2, 3}, []interface{}{2, 3}))
	assert.Equal(t, -1, compareSlices([]interface{}{2, 3}, []interface{}{2, 3, 0}))
	assert.Equal(t, -1, compareSlices([]interface{}{2, 3}, []interface{}{2, 5}))
	assert.Equal(t, 1, compareSlices([]interface{}{2, 4}, []interface{}{2, 3}))
	assert.Equal(t, 1, compareSlices([]interface{}{2, 4, 0}, []interface{}{2, 3}))
	assert.Equal(t, 1, compareSlices([]interface{}{[]interface{}{}}, []interface{}{}))
}

func TestParseFlatList(t *testing.T) {
	assert.Equal(t, []interface{}{1, 2, 3, 4, 5}, parseFlatList("[1,2,3,4,5]"))
	assert.Equal(t, []interface{}{1}, parseFlatList("[1]"))
	assert.Equal(t, []interface{}{}, parseFlatList("[]"))
}

func TestCut(t *testing.T) {
	assert.Equal(t, []string{"1", "2", "3"}, cut("1,2,3"))
	assert.Equal(t, []string{"1", "[2]", "3"}, cut("1,[2],3"))
	assert.Equal(t, []string{"1", "[2,4]", "3"}, cut("1,[2,4],3"))
	assert.Equal(t, []string{"[1,[2,4],3]"}, cut("[1,[2,4],3]"))
}

func TestParseLine(t *testing.T) {
	assert.Equal(t, 12, parseLine("12"))
	assert.Equal(t, []interface{}{5, 4, 3}, parseLine("[5,4,3]"))
	assert.Equal(t, []interface{}{44}, parseLine("[44]"))
	assert.Equal(t, []interface{}{}, parseLine("[]"))
	assert.Equal(t, []interface{}{5, []interface{}{4, 3}, 2}, parseLine("[5,[4,3],2]"))
}
