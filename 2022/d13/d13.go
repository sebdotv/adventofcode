package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"golang.org/x/exp/slices"
	"regexp"
	"strconv"
	"strings"
)

var (
	open       = int32("["[0])
	shut       = int32("]"[0])
	comma      = int32(","[0])
	allNumbers = regexp.MustCompile(`^[0-9]+$`)
	flatList   = regexp.MustCompile(`^\[[0-9,]*]$`)
)

func compareInts(a, b int) int {
	if a == b {
		return 0
	}
	if a < b {
		return -1
	}
	return 1
}

func compareSlices(a []interface{}, b []interface{}) int {
	for i := 0; i < aoc.Min(len(a), len(b)); i++ {
		res := compare(a[i], b[i])
		if res == 0 {
			continue
		}
		return res
	}
	return compareInts(len(a), len(b))
}

func compare(left, right interface{}) int {
	switch leftVal := left.(type) {
	case int:
		switch rightVal := right.(type) {
		case int:
			return compareInts(leftVal, rightVal)
		case []interface{}:
			return compareSlices([]interface{}{leftVal}, rightVal)
		default:
			panic("")
		}
	case []interface{}:
		switch rightVal := right.(type) {
		case int:
			return compareSlices(leftVal, []interface{}{rightVal})
		case []interface{}:
			return compareSlices(leftVal, rightVal)
		default:
			panic("")
		}
	default:
		panic("")
	}
}

func parseFlatList(line string) []interface{} {
	if len(line) == 2 {
		return []interface{}{}
	}
	chunks := strings.Split(line[1:len(line)-1], ",")
	result := make([]interface{}, len(chunks))
	for i, chunk := range chunks {
		result[i], _ = strconv.Atoi(chunk)
	}
	return result
}

func cut(line string) []string {
	result := make([]string, 0)
	currentChunk := strings.Builder{}
	depth := 0
	for _, char := range line {
		switch char {
		case open:
			depth++
			currentChunk.WriteRune(char)
		case shut:
			depth--
			currentChunk.WriteRune(char)
		case comma:
			if depth == 0 {
				result = append(result, currentChunk.String())
				currentChunk.Reset()
			} else {
				currentChunk.WriteRune(char)
			}
		default:
			currentChunk.WriteRune(char)
		}
	}
	result = append(result, currentChunk.String())
	return result
}

func parseLine(line string) interface{} {
	if allNumbers.MatchString(line) {
		result, _ := strconv.Atoi(line)
		return result
	}
	if flatList.MatchString(line) {
		return parseFlatList(line)
	}
	elements := cut(line[1 : len(line)-1])
	result := make([]interface{}, len(elements))
	for i, element := range elements {
		result[i] = parseLine(element)
	}
	return result
}

func readInput(filename string) [][]interface{} {
	fileBlocks := aoc.ReadByBlocks(filename)
	result := make([][]interface{}, len(fileBlocks))
	for i, block := range fileBlocks {
		result[i] = []interface{}{parseLine(block[0]), parseLine(block[1])}
	}
	return result
}

func calc(filename string) int {
	input := readInput(filename)
	result := 0
	for i, pair := range input {
		test := compare(pair[0], pair[1])
		if test == -1 {
			result += i + 1
		}
	}
	return result
}

func readInput2(filename string, dividers []interface{}) []interface{} {
	fileBlocks := aoc.ReadByBlocks(filename)
	result := make([]interface{}, 2*len(fileBlocks))
	for i, block := range fileBlocks {
		result[2*i] = parseLine(block[0])
		result[2*i+1] = parseLine(block[1])
	}
	result = append(result, dividers...)
	return result
}

func calc2(filename string) int {
	div2 := parseLine("[[2]]")
	div6 := parseLine("[[6]]")
	input := readInput2(filename, []interface{}{div2, div6})
	slices.SortFunc(input, func(a, b interface{}) int {
		return compare(a, b)
	})
	var loc2, loc6 int
	for idx, val := range input {
		if test := compare(val, div2); test == 0 {
			loc2 = idx + 1
		}
		if test := compare(val, div6); test == 0 {
			loc6 = idx + 1
		}
	}
	return loc2 * loc6
}

func main() {
	aoc.CheckEquals(13, calc("test.txt"))
	aoc.CheckEquals(140, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
