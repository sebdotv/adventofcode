package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	samples = map[int]struct{}{
		20:  {},
		60:  {},
		100: {},
		140: {},
		180: {},
		220: {},
	}
)

func show(drawn map[aoc.Tuple]struct{}) {
	fmt.Println()
	for l := 0; l < 6; l++ {
		for c := 0; c < 40; c++ {
			if _, lit := drawn[aoc.Tuple{X: c, Y: l}]; lit {
				fmt.Print("■")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

func checkCycle(cycle, x int) int {
	_, sample := samples[cycle]
	if sample {
		return x * cycle
	}
	return 0
}

func calc(filename string) int {
	instrs := aoc.ReadInputLines(filename)
	cycle := 0
	x := 1
	result := 0
	drawn := make(map[aoc.Tuple]struct{})
	for _, instr := range instrs {
		chunks := strings.Split(instr, " ")
		cycle++
		crt := cycle - 1
		result += checkCycle(cycle, x)
		if aoc.Abs(x-(crt%40)) <= 1 {
			drawn[aoc.Tuple{Y: crt / 40, X: crt % 40}] = struct{}{}
		}
		if len(chunks) == 2 {
			cycle++
			crt = cycle - 1
			result += checkCycle(cycle, x)
			if aoc.Abs(x-(crt%40)) <= 1 {
				drawn[aoc.Tuple{Y: crt / 40, X: crt % 40}] = struct{}{}
			}
			n, _ := strconv.Atoi(chunks[1])
			x += n
		}
	}
	show(drawn)
	return result
}

func main() {
	aoc.CheckEquals(13140, calc("test.txt"))
	fmt.Println(calc("input.txt")) // PZGPKPEB
}
