package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	left   = aoc.Tuple{X: -1, Y: 0}
	right  = aoc.Tuple{X: 1, Y: 0}
	down   = aoc.Tuple{X: 0, Y: -1}
	remain = []int{0, 59, 118, 177, 241, 301, 361}
)

type rock struct {
	rockDef  map[aoc.Tuple]struct{}
	blocks   map[aoc.Tuple]struct{}
	width    int
	height   int
	location aoc.Tuple
}

func (r *rock) setLocation(loc aoc.Tuple) {
	r.blocks = make(map[aoc.Tuple]struct{})
	for pos := range r.rockDef {
		r.blocks[pos.Add(loc)] = struct{}{}
	}
	r.location = loc
}

type chamber struct {
	occupied    map[aoc.Tuple]struct{}
	currentRock *rock
	gasJets     []bool
	rocks       []*rock
	maxHeight   int
	rockIdx     int
	gasIdx      int
}

func readRocks() []*rock {
	rockDefinitions := aoc.ReadByBlocks("rocks.txt")
	result := make([]*rock, len(rockDefinitions))
	for idx, rockDef := range rockDefinitions {
		newRock := &rock{
			rockDef:  make(map[aoc.Tuple]struct{}),
			blocks:   make(map[aoc.Tuple]struct{}),
			width:    len(rockDef[0]),
			height:   len(rockDef),
			location: aoc.Tuple{},
		}
		for l, line := range rockDef {
			for c, char := range line {
				if char == int32("#"[0]) {
					newRock.rockDef[aoc.Tuple{X: c, Y: -l}] = struct{}{}
				}
			}
		}
		result[idx] = newRock
	}
	return result
}

func (r *rock) copy() *rock {
	return &rock{
		rockDef:  r.rockDef,
		blocks:   r.blocks,
		width:    r.width,
		height:   r.height,
		location: r.location,
	}
}

func (c *chamber) collide(rockPos *rock) bool {
	for loc := range rockPos.blocks {
		if loc.Y < 0 {
			return true
		}
		if _, occ := c.occupied[loc]; occ {
			return true
		}
	}
	return false
}

func (c *chamber) gasPush() {
	defer func() {
		c.gasIdx++
		if c.gasIdx >= len(c.gasJets) {
			c.gasIdx = 0
		}
	}()
	lateral := left
	canDoMove := c.currentRock.location.X > 0
	if c.gasJets[c.gasIdx] {
		lateral = right
		canDoMove = c.currentRock.location.X+c.currentRock.width < 7
	}
	if canDoMove {
		newRockPos := c.currentRock.location.Add(lateral)
		newRock := c.currentRock.copy()
		newRock.setLocation(newRockPos)
		if !c.collide(newRock) {
			c.currentRock = newRock
		}
	}
}

func (c *chamber) goDown() bool {
	newRockPos := c.currentRock.location.Add(down)
	newRock := c.currentRock.copy()
	newRock.setLocation(newRockPos)
	hasCollided := c.collide(newRock)
	if !hasCollided {
		c.currentRock = newRock
	}
	return hasCollided
}

func (c *chamber) sendRock() {
	defer func() {
		c.rockIdx++
		if c.rockIdx >= len(c.rocks) {
			c.rockIdx = 0
		}
	}()
	newRock := c.rocks[c.rockIdx].copy()
	newRock.setLocation(aoc.Tuple{X: 2, Y: 2 + c.maxHeight + newRock.height})
	c.currentRock = newRock
	//draw(c)
	blocked := false
	for !blocked {
		//draw(c)
		c.gasPush()
		blocked = c.goDown()
	}
	for loc := range c.currentRock.blocks {
		c.occupied[loc] = struct{}{}
		c.maxHeight = aoc.Max(c.maxHeight, loc.Y+1)
	}
	//draw(c)
}

func newChamber(filename string) *chamber {
	rocks := readRocks()
	oneLine := aoc.ReadInputLines(filename)[0]
	gasJets := make([]bool, len(oneLine))
	for i, c := range oneLine {
		gasJets[i] = c == int32(">"[0])
	}
	return &chamber{
		occupied:  make(map[aoc.Tuple]struct{}),
		maxHeight: 0,
		rockIdx:   0,
		gasIdx:    0,
		gasJets:   gasJets,
		rocks:     rocks,
	}
}

func calc(filename string, nbIt int) int {
	c := newChamber(filename)
	ppcm := aoc.Ppcm(len(c.rocks), len(c.gasJets))
	results := make([]int, 0)
	for i := 0; i < nbIt; i++ {
		c.sendRock()
		if i%ppcm == 0 {
			results = append(results, c.maxHeight)
			l := len(results)
			if l > 1 {
				pred := makePred(i, ppcm)
				actual := results[l-1]
				if pred != actual {
					suffix := "   #####"
					fmt.Printf("it %d\tdelta=%d\theight=%d\tprediction=%d%s\n", i, results[l-1]-results[l-2], actual, pred, suffix)
				}
			}
		}
	}
	return c.maxHeight
}

func makePred(i int, ppcm int) int {
	return 432 + (i/(7*ppcm)-1)*424 + remain[(i/ppcm)%7]
}

func main() {
	//fmt.Println(calc("test.txt", 2022))
	fmt.Println(calc("input.txt", 1000000000))
	//fmt.Println(calc("test.txt", 10000000))
	//fmt.Println(makePred(1000000000000, 40))
}
