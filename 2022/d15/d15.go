package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
)

var (
	inputLine = regexp.MustCompile(`Sensor at x=(.*), y=(.*): closest beacon is at x=(.*), y=(.*)`)
)

func readInput(filename string) (map[aoc.Tuple]aoc.Tuple, map[aoc.Tuple]struct{}) {
	closest := make(map[aoc.Tuple]aoc.Tuple)
	lines := aoc.ReadInputLines(filename)
	beacons := make(map[aoc.Tuple]struct{})
	for _, line := range lines {
		matches := inputLine.FindAllStringSubmatch(line, -1)
		for _, subStr := range matches {
			x1, _ := strconv.Atoi(subStr[1])
			y1, _ := strconv.Atoi(subStr[2])
			x2, _ := strconv.Atoi(subStr[3])
			y2, _ := strconv.Atoi(subStr[4])
			beacon := aoc.Tuple{X: x2, Y: y2}
			closest[aoc.Tuple{X: x1, Y: y1}] = beacon
			beacons[beacon] = struct{}{}
		}
	}
	return closest, beacons
}

func calc(filename string, rowNb int) int {
	closest, beacons := readInput(filename)
	noBeaconThere := make(map[int]struct{})
	for sensor, beacon := range closest {
		vector := beacon.Sub(sensor)
		manDist := aoc.Abs(vector.X) + aoc.Abs(vector.Y)
		if aoc.Abs(sensor.Y-rowNb) <= manDist {
			dy := rowNb - sensor.Y
			for dx := -aoc.Abs(manDist - aoc.Abs(dy)); dx <= aoc.Abs(manDist-aoc.Abs(dy)); dx++ {
				xCoord := dx + sensor.X
				_, isBeacon := beacons[aoc.Tuple{X: xCoord, Y: rowNb}]
				if !isBeacon {
					noBeaconThere[xCoord] = struct{}{}
				}
			}
		}
	}
	return len(noBeaconThere)
}

func norm(t aoc.Tuple) int {
	return aoc.Abs(t.X) + aoc.Abs(t.Y)
}

func crown(t aoc.Tuple, sRange, maxCoord int) map[aoc.Tuple]struct{} {
	result := make(map[aoc.Tuple]struct{})
	for dy := -sRange; dy <= sRange; dy++ {
		dx := aoc.Abs(sRange - aoc.Abs(dy))
		newPos := aoc.Tuple{X: t.X + dx, Y: t.Y + dy}
		if newPos.X >= 0 && newPos.X <= maxCoord && newPos.Y >= 0 && newPos.Y <= maxCoord {
			result[newPos] = struct{}{}
		}
		dx = -aoc.Abs(sRange - aoc.Abs(dy))
		newPos = aoc.Tuple{X: t.X + dx, Y: t.Y + dy}
		if newPos.X >= 0 && newPos.X <= maxCoord && newPos.Y >= 0 && newPos.Y <= maxCoord {
			result[newPos] = struct{}{}
		}
	}
	return result
}

func calc2(filename string, maxCoord int) int {
	closest, _ := readInput(filename)
	sensorRanges := make(map[aoc.Tuple]int)
	for sensor, beacon := range closest {
		sensorRanges[sensor] = norm(beacon.Sub(sensor))
	}
	candidates := make(map[aoc.Tuple]struct{})
	eliminated := make(map[aoc.Tuple]struct{})
	for sensor, sRange := range sensorRanges {
		aroundSensor := crown(sensor, sRange+1, maxCoord)
		for otherSensor, otherRange := range sensorRanges {
			for pos := range aroundSensor {
				if norm(pos.Sub(otherSensor)) <= otherRange {
					eliminated[pos] = struct{}{}
					delete(candidates, pos)
					continue
				} else {
					if _, wasEliminated := eliminated[pos]; !wasEliminated {
						candidates[pos] = struct{}{}
					}
				}
			}
		}
		if len(candidates) > 0 {
			for candidate := range candidates {
				return candidate.X*4000000 + candidate.Y
			}
		}
	}
	return 0
}

func main() {
	aoc.CheckEquals(26, calc("test.txt", 10))
	fmt.Println(calc("input.txt", 2000000))
	aoc.CheckEquals(56000011, calc2("test.txt", 20))
	fmt.Println(calc2("input.txt", 4000000))
}
