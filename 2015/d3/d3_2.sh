#!/bin/bash

X=1000
Y=1000
XR=1000
YR=1000
DB="db.txt"
rm -f "$DB"
echo "1000,1000" > "$DB"

TURN=0

cat in.txt | while IFS= read -r -n1 char; do
    TURN=$((1-TURN))
    if [ "$TURN" -eq 1 ]; then
        case $char in
            '>')
                X=$((X+1))
                ;;
            '<')
                X=$((X-1))
                ;;
            'v')
                Y=$((Y-1))
                ;;
            '^')
                Y=$((Y+1))
                ;;
        esac
        if ! grep -q "$X,$Y" "$DB"; then
            echo "$X,$Y" >> "$DB"
        fi
    else
        case $char in
            '>')
                XR=$((XR+1))
                ;;
            '<')
                XR=$((XR-1))
                ;;
            'v')
                YR=$((YR-1))
                ;;
            '^')
                YR=$((YR+1))
                ;;
        esac
        if ! grep -q "$XR,$YR" "$DB"; then
            echo "$XR,$YR" >> "$DB"
        fi
    fi
done
wc -l "$DB"
