package d19;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D19_2 {

	private static Map<String, String> transformations = new HashMap<String, String>();

	private static final Pattern P = Pattern.compile("([a-zA-Z]*) => ([a-zA-Z]*)");

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			// input
			File f = new File("sample");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					String t_from = m.group(1);
					String t_to = m.group(2);
					transformations.put(t_to, t_from);
				} else {
					System.err.println("ERROR: '" + line + "' does not match the expected pattern");
				}
			}
			b.close();

			// molecule
			f = new File("molecule");
			b = new BufferedReader(new FileReader(f));
			String target = b.readLine();
			target = "HOHOHO";
			b.close();

			String molecule = "e";
			List<String> first = new ArrayList<String>();
			first.add(molecule);

			int result = recurseOn(first, target, 1);
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static int recurseOn(List<String> list, String target, int iterations) {
		System.out.println("Iteration " + iterations + "; processing " + list.size() + " molecules...");
		List<String> solutions = new ArrayList<String>();
		for (String molecule : list) {
			for (Entry<String, String> t : transformations.entrySet()) {
				String t_from = t.getValue();
				String t_to = t.getKey();
				int instanceIdx = molecule.indexOf(t_from, 0);
				while (instanceIdx >= 0) {
					String result = molecule.substring(0, instanceIdx) + t_to
							+ molecule.substring(instanceIdx + t_from.length());
					if (target.equals(result)) {
						return iterations;
					}
					if (!solutions.contains(result))
						solutions.add(result);
					instanceIdx = molecule.indexOf(t_from, ++instanceIdx);
				}
			}
		}

		return recurseOn(solutions, target, iterations + 1);
	}
}
