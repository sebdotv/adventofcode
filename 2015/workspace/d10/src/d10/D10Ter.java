package d10;

import java.util.regex.Pattern;

public class D10Ter {

	private static final Pattern P3 = Pattern.compile("(.)\\1\\1");
	private static final Pattern P2 = Pattern.compile("(.)\\1");

	private static final String INPUT_NB = "1321131112";

	public static void main(String[] args) {
		System.out.println(" --> " + iterate(INPUT_NB, 49).length());
	}

	private static String iterate(String inputStr, int remaining) {
		System.out.print(remaining + ": ");
		boolean finished = false;
		int i = 0;
		String result = "";
		while (!finished) {
			String curNb = "";
			String curNb2 = "";
			String curNb3 = "";
			try {
				curNb = inputStr.substring(i, i + 1);
				if (i < inputStr.length() - 1)
					curNb2 = inputStr.substring(i, i + 2);
				if (i < inputStr.length() - 2)
					curNb3 = inputStr.substring(i, i + 3);
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
			} finally {

				boolean processFurther = true;
				if (curNb3 != "") {
					if (P3.matcher(curNb3).matches()) {
						processFurther = false;
						result += "3" + curNb;
						i += 3;
						if (i >= inputStr.length())
							finished = true;
					}
				}

				if (processFurther) {
					if (curNb2 != "") {
						if (P2.matcher(curNb2).matches()) {
							processFurther = false;
							result += "2" + curNb;
							i += 2;
							if (i >= inputStr.length())
								finished = true;
						}
					}

					if (processFurther) {
						result += "1" + curNb;
						i++;
						if (i >= inputStr.length())
							finished = true;
					}
				}
			}
		}
		if (remaining == 0)
			return result;

		System.out.println(result.length());
		return iterate(result, remaining - 1);
	}
}
