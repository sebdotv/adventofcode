package d10;

public class D10Bis {

	private static final String IN = "1321131112";

	public static void main(String[] args) {
		System.out.println(IN);
		System.out.println(iterate(IN, 1));
	}

	private static String iterate(String inputStr, int remaining) {
		String curNb = "";
		String prevNb = "";
		String prevPrevNb = "";
		String result = "";
		for (char curChar : inputStr.toCharArray()) {
			result += prevPrevNb;
			prevPrevNb = prevNb;
			prevNb = curNb;
			curNb = String.valueOf(curChar);
			if (prevPrevNb.equals(prevNb) && prevNb.equals(curNb) && curNb != "") {
				prevPrevNb = "3" + prevPrevNb;
				prevNb = "";
				curNb = "";
			} else if (prevPrevNb.equals(prevNb) && prevNb != "") {
				prevPrevNb = "2" + prevNb;
				prevNb = "";
			} else if (prevPrevNb != "") {
				prevPrevNb = "1" + prevPrevNb;
			}
		}
		if (prevPrevNb.equals(prevNb) && prevNb.equals(curNb) && curNb != "") {
			result += "3" + prevPrevNb;
		} else if (curNb.equals(prevNb) && prevNb != "") {
			result += "2" + prevNb;
		} else {
			result += "1" + prevNb;
			result += "1" + curNb;
		}
		if (remaining == 0)
			return result;

		System.out.println(result);
		return iterate(result, remaining - 1);
	}
}
