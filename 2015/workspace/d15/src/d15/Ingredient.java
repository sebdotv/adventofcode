package d15;

public class Ingredient {
	private String name;

	private Integer capacity;
	private Integer durability;
	private Integer flavor;
	private Integer texture;
	private Integer calories;

	public Ingredient(String name, Integer capacity, Integer durability, Integer flavor, Integer texture,
			Integer calories) {

		this.name = name;
		this.capacity = capacity;
		this.durability = durability;
		this.flavor = flavor;
		this.texture = texture;
		this.calories = calories;

		System.out.println("Added ingredient to kitchen: " + toString());
	}

	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getDurability() {
		return durability;
	}

	public void setDurability(Integer durability) {
		this.durability = durability;
	}

	public Integer getFlavor() {
		return flavor;
	}

	public void setFlavor(Integer flavor) {
		this.flavor = flavor;
	}

	public Integer getTexture() {
		return texture;
	}

	public void setTexture(Integer texture) {
		this.texture = texture;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}
}
