#!/usr/bin/python

def exec_instructions(instructions, progs):
    l = len(progs)
    for i in instructions:
        if i[0] == 's':
            n = int(i[1:])
            progs = progs[l-n:] + progs[:l-n]
        elif i[0] == 'x':
            [a, b] = list(map(int, i[1:].split('/')))
            pa = progs[a]
            pb = progs[b]
            progs[a] = pb
            progs[b] = pa
        else:
            [a, b] = i[1:].split('/')
            ia = progs.index(a)
            ib = progs.index(b)
            progs[ia] = b
            progs[ib] = a
    return progs

def calc2(instructions, p16):
    period = 0
    history = {0: ''.join(p16)}
    progs = p16.copy()
    while True:
        period += 1
        progs = exec_instructions(instructions, progs.copy())
        if progs == p16:
            return history[1000000000 % period]
        history[period] = ''.join(progs)

if __name__ == '__main__':
    with open('test', 'r') as t:
        test_instructions = t.read().strip().split(',')
        test_progs = list(map(chr, range(97, 102)))
        assert ''.join(exec_instructions(test_instructions, test_progs)) == 'baedc'
    with open('input', 'r') as f:
        instructions = f.read().strip().split(',')
        p16 = list(map(chr, range(97, 113)))
        progs = exec_instructions(instructions, p16.copy())
        print('Answer 1: %s' % ''.join(progs))
        print('Answer 2: %s' % calc2(instructions, p16.copy()))

