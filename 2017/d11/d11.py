#!/usr/bin/python

import numpy as np

test = {
    'ne,ne,ne': (3, 3),
    'ne,ne,sw,sw': (0, 2),
    'ne,ne,s,s': (2, 2),
    'se,sw,se,sw,sw': (3, 3)
}

# { move: [y, x] }
coords = {
    's': np.array([-2, 0]),
    'n': np.array([2, 0]),
    'se': np.array([-1, 1]),
    'sw': np.array([-1, -1]),
    'ne': np.array([1, 1]),
    'nw': np.array([1, -1])
}


def distance(position):
    yx = np.abs(position)
    v_shift = int(max(0, yx[0] - yx[1]) / 2)
    return v_shift + yx[1]


def calc(in_str):
    yx = np.array([0, 0])
    moves = in_str.split(',')
    max_distance = 0
    for move in moves:
        yx += coords[move]
        max_distance = max(max_distance, distance(yx))
    return (distance(yx), max_distance)


if __name__ == '__main__':
    for i in test:
        c = calc(i)
        # print('%s | %s | %s' % (test[i], c, i))
        assert c == test[i]
    with open('input', 'r') as f:
        in_str = f.read().strip()
        print('answers: %d, %d' % calc(in_str))
