#!/usr/bin/python

from itertools import islice, cycle

test = { 'in_list_size': 5, 'lengths': [3, 4, 1, 5], 'result': 12 }
test2 = {
        '': 'a2582a3a0e66e6e86e3812dcb672a272',
        'AoC 2017': '33efeb34ea91902bb2f59c9920caa6cd',
        '1,2,3': '3efbe78a8d82f29979031a4aa0b16a9d',
        '1,2,4': '63960835bcdc130f0b66d7ff4f6a5a8e'
        }

def xor_hash(l):
    h = 0
    for i in l:
        h ^= i
    return h

def twist(in_list, lengths, cur_pos, skip_size):
    L = len(in_list)
    for l in lengths:
        seq_a = list(islice(cycle(in_list), cur_pos, cur_pos + l))[::-1]
        seq_b = list(islice(cycle(in_list), cur_pos + l, cur_pos + L))
        in_list = list(islice(cycle(seq_a + seq_b), -cur_pos % L, L + (-cur_pos % L)))
        cur_pos += l + skip_size
        cur_pos %= L
        skip_size += 1
    return (in_list, cur_pos, skip_size)

def calc(L, lengths):
    result = twist(list(range(L)), lengths, 0, 0)
    a1 = result[0][0] * result[0][1]
    return a1

def calc2(L, in_str):
    lengths = list(map(ord, in_str))
    lengths  += [17, 31, 73, 47, 23]
    in_list = list(range(L))
    cur_pos = 0
    skip_size = 0
    for _ in range(64):
        (in_list, cur_pos, skip_size) = twist(in_list, lengths, cur_pos, skip_size)
    in_list = [in_list[i:i+16] for i in range(0, len(in_list), 16)]
    in_list = list(map(xor_hash, in_list))
    result = ''.join('%02x' % x for x in in_list)
    return result

if __name__ == '__main__':
    # print('%d | %d :: %d | %s' % (test['in_list_size'], test['result'], calc(test['in_list_size'],
    #    test['lengths']), test['lengths']))
    assert calc(test['in_list_size'], test['lengths']) == test['result']
    for t in test2:
        tt = calc2(256, t)
        # print('%10s | %s | %s' % (t, tt, test2[t]))
        assert test2[t] == tt
    with open('input', 'r') as f:
        in_str = f.read().strip()
        lengths = list(map(int, in_str.split(',')))
        print('answer 1: %d' % calc(256, lengths))
        print('answer 2: %s' % calc2(256, in_str))
