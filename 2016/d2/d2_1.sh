#!/bin/zsh
# 1 2 3
# 4 5 6
# 7 8 9
X=2
Y=2
cat input | while read line; do
    for i in $(seq 1 ${#line[@]}); do
        case ${line[$i]} in
            U)
                Y=$((Y-1))
                if [ $Y -lt 0 ]; then
                    Y=0
                fi
                ;;
            D)
                Y=$((Y+1))
                if [ $Y -gt 2 ]; then
                    Y=2
                fi
                ;;
            L)
                X=$((X-1))
                if [ $X -lt 1 ]; then
                    X=1
                fi
                ;;
            R)
                X=$((X+1))
                if [ $X -gt 3 ]; then
                    X=3
                fi
                ;;
        esac
    done
    echo "$((X+3*Y))"
done
