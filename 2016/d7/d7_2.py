from re import compile
bab = compile(r'(.)(.)\1')
def bab_to_aba(in_str):
    return in_str[1] + in_str[0] + in_str[1]
def find_babs(in_str):
    answer = []
    h2 = ''
    h1 = ''
    for h0 in in_str:
        m = bab.match(h2 + h1 + h0)
        if m and h1 != h2:
            answer.append(m.group())
        h2 = h1
        h1 = h0
    return answer
if __name__ == '__main__':
    answer = 0
    with open('input', 'r') as f:
        for ip in f:
            parts = [ [], [] ]
            snet_babs = []
            flag = 0
            for i in ip.split('['):
                for j in i.split(']'):
                    parts[flag].append(j.strip())
                    flag = 1 - flag
            for snet in parts[0]:
                for s in find_babs(snet):
                    snet_babs.append(s)
            for hnet in parts[1]:
                for s in find_babs(hnet):
                    if bab_to_aba(s) in snet_babs:
                        answer += 1
                        break
    print(answer)
