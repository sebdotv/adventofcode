package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strings"
)

var finishedPaths *aoc.Set[string]
var currentPaths *aoc.Set[string]

func readInput(filename string) (map[string][]string, *aoc.Set[string]) {
	connections := make(map[string][]string)
	smallCaves := aoc.NewSet[string]()
	lines := aoc.ReadInputLines(filename)
	for _, l := range lines {
		cavePair := strings.Split(l, "-")
		if len(cavePair) != 2 {
			panic(fmt.Sprintf("got %d caves on line '%s' instead of 2", len(cavePair), l))
		}
		c0 := cavePair[0]
		c1 := cavePair[1]
		_, f0 := connections[c0]
		_, f1 := connections[c1]
		if f0 {
			connections[c0] = append(connections[c0], c1)
		} else {
			connections[c0] = []string{c1}
		}
		if f1 {
			connections[c1] = append(connections[c1], c0)
		} else {
			connections[c1] = []string{c0}
		}
		if int(c0[0]) >= 97 {
			smallCaves.Add(c0)
		}
		if int(c1[0]) >= 97 {
			smallCaves.Add(c1)
		}
	}
	return connections, smallCaves
}

func findNeighbours(currentPath string, connections map[string][]string, currentCave string, twoVisits string) []string {
	result := make([]string, 0)
	visitedSmallCaves := make([]string, 0)
	firstVisitDone := twoVisits == ""
	for _, cave := range strings.Split(currentPath, "-") {
		if cave[0] > 96 {
			if cave == twoVisits {
				if firstVisitDone {
					visitedSmallCaves = append(visitedSmallCaves, cave)
				} else {
					firstVisitDone = true
				}
			} else {
				visitedSmallCaves = append(visitedSmallCaves, cave)
			}
		}
	}
	for _, adjCave := range connections[currentCave] {
		if !aoc.Contains(visitedSmallCaves, adjCave) {
			result = append(result, adjCave)
		}
	}
	return result
}

func popOne(stringSet *aoc.Set[string]) string {
	var val string
	for val = range stringSet.Iterate() {
		break
	}
	stringSet.Remove(val)
	return val
}

func getLastCave(path string) string {
	idx := strings.LastIndex(path, "-")
	return path[idx+1:]
}

func depthExplore(connections map[string][]string, twoVisits string) {
	for currentPaths.Size() > 0 {
		pathToContinue := popOne(currentPaths)
		currentCave := getLastCave(pathToContinue)
		if currentCave != "end" {
			possibilities := findNeighbours(pathToContinue, connections, currentCave, twoVisits)
			for _, p := range possibilities {
				newPath := fmt.Sprintf("%s-%s", pathToContinue, p)
				currentPaths.Add(newPath)
			}
		} else {
			finishedPaths.Add(pathToContinue)
		}
	}
}

func calc(filename string) int {
	finishedPaths = aoc.NewSet[string]()
	connections, _ := readInput(filename)
	currentPaths = aoc.NewSet[string]()
	currentPaths.Add("start")
	depthExplore(connections, "")
	return finishedPaths.Size()
}

func calc2(filename string) int {
	finishedPaths = aoc.NewSet[string]()
	connections, smallCaves := readInput(filename)
	for smallCave := range smallCaves.Iterate() {
		if smallCave != "start" && smallCave != "end" {
			currentPaths = aoc.NewSet[string]()
			currentPaths.Add("start")
			depthExplore(connections, smallCave)
		}
	}
	return finishedPaths.Size()
}

func main() {
	aoc.CheckEquals(10, calc("test.txt"))
	aoc.CheckEquals(19, calc("test2.txt"))
	aoc.CheckEquals(226, calc("test3.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(36, calc2("test.txt"))
	aoc.CheckEquals(103, calc2("test2.txt"))
	aoc.CheckEquals(3509, calc2("test3.txt"))
	fmt.Println(calc2("input.txt"))
}
