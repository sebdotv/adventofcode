package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "os"
    "regexp"
    "runtime/pprof"
    "strconv"
)

var (
    regexNums       = regexp.MustCompile(`(\d+)`)
    regexLastNumber = regexp.MustCompile(`.*[^\d](\d+).*`)
    regexSimplePair = regexp.MustCompile(`\[(\d+),(\d+)]`)
)

func getLastNumber(inStr string) (int, []int) {
    numbers := regexLastNumber.FindStringSubmatchIndex(inStr)
    if len(numbers) != 0 {
        lastNumberIdx := numbers[2:]
        chunk := inStr[lastNumberIdx[0]:lastNumberIdx[1]]
        nbr, _ := strconv.ParseInt(chunk, 10, 64)
        resultIndexes := []int{lastNumberIdx[0], lastNumberIdx[1]}
        return int(nbr), resultIndexes
    }
    return -1, []int{}
}

func getFirstNumber(inStr string, offset int) (int, []int) {
    firstNumberIdx := regexNums.FindStringSubmatchIndex(inStr)
    if firstNumberIdx != nil {
        firstNumberIdx = firstNumberIdx[2:]
        chunk := inStr[firstNumberIdx[0]:firstNumberIdx[1]]
        nbr, _ := strconv.ParseInt(chunk, 10, 64)
        resultIndexes := []int{firstNumberIdx[0] + offset, firstNumberIdx[1] + offset}
        return int(nbr), resultIndexes
    }
    return -1, []int{}
}

func replacePortion(inStr string, replacement string, indexes []int) string {
    p1 := inStr[:indexes[0]]
    p3 := inStr[indexes[1]:]
    result := fmt.Sprintf("%s%s%s", p1, replacement, p3)
    return result
}

func explodePair(numberStr string, index int) string {
    nbBefore, beforeCoords := getLastNumber(numberStr[:index])
    left, leftCoords := getFirstNumber(numberStr[index:], index)
    right, rightCoords := getFirstNumber(numberStr[leftCoords[1]:], leftCoords[1])
    nbAfter, afterCoords := getFirstNumber(numberStr[rightCoords[1]:], rightCoords[1])
    explodingPairCoords := []int{index, rightCoords[1] + 1}

    if nbAfter > -1 {
        numberStr = replacePortion(numberStr, fmt.Sprintf("%d", nbAfter+right), afterCoords)
    }
    numberStr = replacePortion(numberStr, "0", explodingPairCoords)
    if nbBefore > -1 {
        numberStr = replacePortion(numberStr, fmt.Sprintf("%d", nbBefore+left), beforeCoords)
    }
    return numberStr
}

func splitNumber(numberStr string, number int, index []int) string {
    a := number / 2
    b := (number + 1) / 2
    result := fmt.Sprintf("%s[%d,%d]%s", numberStr[:index[0]], a, b, numberStr[index[1]:])
    return result
}

func reduce(numberStr string) string {
    embedLevel := 0
    for index, char := range numberStr {
        if char == 91 { // [
            embedLevel++
        } else if char == 93 { // ]
            embedLevel--
        }
        if embedLevel == 5 {
            return explodePair(numberStr, index)
        }
    }
    for index, char := range numberStr {
        if char >= 48 && char <= 57 {
            nextNb, nbCoords := getFirstNumber(numberStr[index:], index)
            if nextNb > 9 {
                return splitNumber(numberStr, nextNb, nbCoords)
            }
        }
    }
    return numberStr
}

func add(n1, n2 string) string {
    currentStr := fmt.Sprintf("[%s,%s]", n1, n2)
    nextStr := reduce(currentStr)
    for nextStr != currentStr {
        currentStr = nextStr
        nextStr = reduce(currentStr)
    }
    return nextStr
}

func computeMagnitude(pairStr string) string {
    pairNumbers := regexSimplePair.FindStringSubmatch(pairStr)
    left, _ := strconv.ParseInt(pairNumbers[1], 10, 64)
    right, _ := strconv.ParseInt(pairNumbers[2], 10, 64)
    return fmt.Sprintf("%d", 3*left+2*right)
}

func magnitude(numberStr string) int {
    for regexSimplePair.MatchString(numberStr) {
        numberStr = regexSimplePair.ReplaceAllStringFunc(numberStr, computeMagnitude)
    }
    finalNumber, _ := strconv.ParseInt(numberStr, 10, 64)
    return int(finalNumber)
}

func calc(filename string) int {
    lines := aoc.ReadInputLines(filename)
    currentNumberStr := lines[0]
    for i := 1; i < len(lines); i++ {
        line := lines[i]
        currentNumberStr = add(currentNumberStr, line)
    }
    return magnitude(currentNumberStr)
}

func calc2(filename string) int {
    lines := aoc.ReadInputLines(filename)
    maxMag := 0
    for _, l1 := range lines {
        for _, l2 := range lines {
            if l1 != l2 {
                maxMag = aoc.Max(maxMag, magnitude(add(l1, l2)))
                maxMag = aoc.Max(maxMag, magnitude(add(l2, l1)))
            }
        }
    }
    return maxMag
}

func main() {
    f, err := os.Create("pprof_cpu.prof")
    if err != nil {
        panic(err)
    }
    defer f.Close()
    if err2 := pprof.StartCPUProfile(f); err2 != nil {
        panic(err2)
    }
    defer pprof.StopCPUProfile()
    aoc.CheckEquals(4140, calc("test.txt"))
    aoc.CheckEquals(3993, calc2("test.txt"))
    fmt.Println(calc("input.txt"))
    fmt.Println(calc2("input.txt"))
}
