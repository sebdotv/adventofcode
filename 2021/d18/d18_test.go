package main

import (
	"testing"
)

var explodeData = map[string]struct {
	expected string
	index    int
}{
	"[[[[[9,8],1],2],3],4]":                 {index: 4, expected: "[[[[0,9],2],3],4]"},
	"[7,[6,[5,[4,[3,2]]]]]":                 {index: 12, expected: "[7,[6,[5,[7,0]]]]"},
	"[[6,[5,[4,[3,2]]]],1]":                 {index: 10, expected: "[[6,[5,[7,0]]],3]"},
	"[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]": {index: 10, expected: "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"},
	"[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]":     {index: 24, expected: "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"},
}

var splitData = map[string]struct {
	expected string
	coords   []int
	number   int
}{
	"[[[[0,7],4],[15,[0,13]]],[1,1]]":    {number: 15, coords: []int{13, 15}, expected: "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]"},
	"[[[[0,7],4],[[7,8],[0,13]]],[1,1]]": {number: 13, coords: []int{22, 24}, expected: "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]"},
}

var addData = map[string][]string{
	"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]":                             {"[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]"},
	"[[[[1,1],[2,2]],[3,3]],[4,4]]":                                 {"[1,1]", "[2,2]", "[3,3]", "[4,4]"},
	"[[[[3,0],[5,3]],[4,4]],[5,5]]":                                 {"[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]"},
	"[[[[5,0],[7,4]],[5,5]],[6,6]]":                                 {"[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"},
	"[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]":     {"[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]", "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]"},
	"[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]": {"[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]", "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]"},
	"[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]": {"[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]", "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]"},
	"[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]": {"[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]", "[7,[5,[[3,8],[1,4]]]]"},
	"[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]":     {"[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]", "[[2,[2,2]],[8,[8,1]]]"},
	"[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]":             {"[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]", "[2,9]"},
	"[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]": {"[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]", "[1,[[[9,3],9],[[9,0],[0,7]]]]"},
	"[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]":             {"[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]", "[[[5,[7,4]],7],1]"},
	"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]":         {"[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]", "[[[[4,2],2],6],[8,7]]"},
}

var magnitudeData = map[string]int{
	"[9,1]":                             29,
	"[1,9]":                             21,
	"[[9,1],[1,9]]":                     129,
	"[[1,2],[[3,4],5]]":                 143,
	"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]": 1384,
	"[[[[1,1],[2,2]],[3,3]],[4,4]]":     445,
	"[[[[3,0],[5,3]],[4,4]],[5,5]]":     791,
	"[[[[5,0],[7,4]],[5,5]],[6,6]]":     1137,
	"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]": 3488,
}

func TestExplode(t *testing.T) {
	atLeast1Error := false
	for inStr, idxAndExp := range explodeData {
		result := explodePair(inStr, idxAndExp.index)
		if result != idxAndExp.expected {
			t.Errorf("exp: %s\ngot: %s\n\n", idxAndExp.expected, result)
			atLeast1Error = true
		}
	}
	if atLeast1Error {
		t.Fail()
	}
}

func TestSplit(t *testing.T) {
	atLeast1Error := false
	for inStr, params := range splitData {
		result := splitNumber(inStr, params.number, params.coords)
		if result != params.expected {
			t.Errorf("exp: %s\ngot: %s\n\n", params.expected, result)
			atLeast1Error = true
		}
	}
	if atLeast1Error {
		t.Fail()
	}
}

func TestReduce(t *testing.T) {
	atLeast1Error := false
	for inStr, idxAndExp := range explodeData {
		result := reduce(inStr)
		if result != idxAndExp.expected {
			t.Errorf("exp: %s\ngot: %s\n\n", idxAndExp.expected, result)
			atLeast1Error = true
		}
	}
	if atLeast1Error {
		t.Fail()
	}
}

func TestAdd(t *testing.T) {
	nbErrors := 0
	for exp, params := range addData {
		currentStr := params[0]
		for i, p := range params {
			if i > 0 {
				currentStr = add(currentStr, p)
			}
		}
		if currentStr != exp {
			t.Errorf("sum(%v)\nexp: %s\ngot: %s\n\n", params, exp, currentStr)
			nbErrors++
		}
	}
	if nbErrors > 0 {
		t.Errorf("%d sums out of %d failed", nbErrors, len(addData))
		t.Fail()
	}
}

func TestMagnitude(t *testing.T) {
	atLeast1Error := false
	for inStr, expMag := range magnitudeData {
		mag := magnitude(inStr)
		if expMag != mag {
			atLeast1Error = true
			t.Errorf("magnitude for %s: expected %d, got %d", inStr, expMag, mag)
		}
	}
	if atLeast1Error {
		t.Fail()
	}
}
