package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"os"
	"regexp"
	"runtime/pprof"
	"strings"
)

type hallwayPosition struct {
	location int // 0 is leftmost, 10 is rightmost
}

type stashedPosition struct {
	corridor int // 0 is leftmost, 3 is rightmost
	slot     int // 0 is below, 1 is above
}

type currentSituation struct {
	stashedAmphipods map[stashedPosition]string
	hallwayAmphipods map[hallwayPosition]string
	signature        string
	score            int
}

type movementOut struct {
	from stashedPosition
	to   hallwayPosition
}

type movementIn struct {
	from hallwayPosition
	to   stashedPosition
}

var (
	moveCost = map[string]int{
		"A": 1,
		"B": 10,
		"C": 100,
		"D": 1000,
	}
	aboveRowRE      = regexp.MustCompile(`^###(.)#(.)#(.)#(.)###$`)
	belowRowRE      = regexp.MustCompile(`^#(.)#(.)#(.)#(.)#$`)
	targetSituation = "AABBCCDD..........."
)

func corridorLocation(corridorNb int) int {
	return 2*corridorNb + 2
}

func (s *currentSituation) equals(s2 *currentSituation) bool {
	return s.signature == s2.signature
}

func readInput(filename string) *currentSituation {
	lines := aoc.ReadInputLines(filename)
	amphipodsAbove := aboveRowRE.FindAllStringSubmatch(lines[2], -1)[0]
	amphipodsBelow := belowRowRE.FindAllStringSubmatch(lines[3], -1)[0]
	stashed := make(map[stashedPosition]string)
	for i, a := range amphipodsAbove {
		if i > 0 {
			stashed[stashedPosition{
				corridor: i - 1,
				slot:     1,
			}] = a
		}
	}
	for i, a := range amphipodsBelow {
		if i > 0 {
			stashed[stashedPosition{
				corridor: i - 1,
				slot:     0,
			}] = a
		}
	}
	return &currentSituation{
		stashedAmphipods: stashed,
		hallwayAmphipods: make(map[hallwayPosition]string),
		signature:        genSignature(stashed, make(map[hallwayPosition]string)),
	}
}

func (s *currentSituation) moveOut(from stashedPosition, to hallwayPosition) int {
	corLoc := corridorLocation(from.corridor)
	score := (2-from.slot)*moveCost[s.stashedAmphipods[from]] + aoc.Abs(corLoc-to.location)*moveCost[s.stashedAmphipods[from]]
	s.hallwayAmphipods[to] = s.stashedAmphipods[from]
	delete(s.stashedAmphipods, from)
	s.signature = genSignature(s.stashedAmphipods, s.hallwayAmphipods)
	return score
}

func (s *currentSituation) moveOutIsValid(from stashedPosition, to hallwayPosition) bool {
	if _, isAmphipod := s.stashedAmphipods[from]; !isAmphipod {
		// source must be amphipod
		return false
	}
	if _, isAmphipod := s.hallwayAmphipods[to]; isAmphipod {
		// destination must not be amphipod
		return false
	}
	for c := 0; c <= 3; c++ {
		corLoc := corridorLocation(c)
		if to.location == corLoc {
			// can't block entrance
			return false
		}
	}
	corLoc := corridorLocation(from.corridor)
	if from.slot == 0 {
		// can't jump over other amphipod
		if _, blocked := s.stashedAmphipods[stashedPosition{
			corridor: from.corridor,
			slot:     1,
		}]; blocked {
			return false
		}
	}
	left := aoc.Min(corLoc, to.location)
	right := aoc.Max(corLoc, to.location)
	for loc := left; loc <= right; loc++ {
		_, blocked := s.hallwayAmphipods[hallwayPosition{loc}]
		if blocked {
			// can't jump over other amphipod
			return false
		}
	}
	return true
}

func (s *currentSituation) moveIn(from hallwayPosition, to stashedPosition) int {
	span := aoc.Abs(from.location - corridorLocation(to.corridor))
	letter := s.hallwayAmphipods[from]
	score := (2 - to.slot + span) * moveCost[letter]
	s.stashedAmphipods[to] = s.hallwayAmphipods[from]
	delete(s.hallwayAmphipods, from)
	s.signature = genSignature(s.stashedAmphipods, s.hallwayAmphipods)
	return score
}

func (s *currentSituation) moveInIsValid(from hallwayPosition, to stashedPosition) bool {
	if _, isAmphipod := s.hallwayAmphipods[from]; !isAmphipod {
		// source must be amphipod
		return false
	}
	if _, isAmphipod := s.stashedAmphipods[to]; isAmphipod {
		// destination must not be amphipod
		return false
	}
	letter := s.hallwayAmphipods[from]
	correctCorridor := int(letter[0] - 65)
	if correctCorridor != to.corridor {
		// Can't move into wrong corridor
		return false
	}
	_, belowFull := s.stashedAmphipods[stashedPosition{
		corridor: to.corridor,
		slot:     0,
	}]
	_, aboveFull := s.stashedAmphipods[stashedPosition{
		corridor: to.corridor,
		slot:     1,
	}]
	if belowFull && aboveFull {
		// Can't move into full corridor
		return false
	}
	corLoc := corridorLocation(to.corridor)
	left := aoc.Min(corLoc, from.location)
	right := aoc.Max(corLoc, from.location)
	for loc := left; loc <= right; loc++ {
		_, blocked := s.hallwayAmphipods[hallwayPosition{loc}]
		if blocked && loc != from.location {
			// can't jump over other amphipod
			return false
		}
	}
	_, someoneBelow := s.stashedAmphipods[stashedPosition{
		corridor: to.corridor,
		slot:     0,
	}]
	if to.slot == 1 && !someoneBelow {
		// Can't go to above slot if below isn't occupied
		return false
	}
	return true
}

func (s *currentSituation) possibleMovements() ([]*movementIn, []*movementOut) {
	possibleMoveIns := make([]*movementIn, 0)
	possibleMoveOuts := make([]*movementOut, 0)
	for from, letter := range s.hallwayAmphipods {
		correctCorridor := int(letter[0] - 65)
		for slot := 0; slot <= 1; slot++ {
			to := stashedPosition{
				corridor: correctCorridor,
				slot:     slot,
			}
			if s.moveInIsValid(from, to) {
				possibleMoveIns = append(possibleMoveIns, &movementIn{
					from: from,
					to:   to,
				})
			}
		}
	}
	for from := range s.stashedAmphipods {
		for dst := 0; dst <= 10; dst++ {
			to := hallwayPosition{location: dst}
			if s.moveOutIsValid(from, to) {
				possibleMoveOuts = append(possibleMoveOuts, &movementOut{
					from: from,
					to:   to,
				})
			}
		}
	}
	return possibleMoveIns, possibleMoveOuts
}

func genSignature(stashedAmphipods map[stashedPosition]string, hallwayAmphipods map[hallwayPosition]string) string {
	result := strings.Builder{}
	for c := 0; c <= 3; c++ {
		for s := 0; s <= 1; s++ {
			l, x := stashedAmphipods[stashedPosition{
				corridor: c,
				slot:     s,
			}]
			if !x {
				l = "."
			}
			result.WriteString(l)
		}
	}
	for p := 0; p <= 10; p++ {
		l, x := hallwayAmphipods[hallwayPosition{location: p}]
		if !x {
			l = "."
		}
		result.WriteString(l)
	}
	return result.String()
}

func (s *currentSituation) deepCopy() *currentSituation {
	result := &currentSituation{
		stashedAmphipods: make(map[stashedPosition]string),
		hallwayAmphipods: make(map[hallwayPosition]string),
		score:            s.score,
		signature:        s.signature,
	}
	for pos, l := range s.stashedAmphipods {
		result.stashedAmphipods[pos] = l
	}
	for pos, l := range s.hallwayAmphipods {
		result.hallwayAmphipods[pos] = l
	}
	return result
}

func appendIfNotVisited(visited []*currentSituation, sCopy *currentSituation) []*currentSituation {
	for _, s := range visited {
		if s.equals(sCopy) {
			return visited
		}
	}
	visited = append(visited, sCopy)
	return visited
}

func scheduleVisit(history []*currentSituation, sCopy *currentSituation, visited []*currentSituation) []*currentSituation {
	for _, s := range visited {
		if s.equals(sCopy) {
			return history
		}
	}
	for _, s := range history {
		if s.equals(sCopy) {
			return history
		}
	}
	history = append(history, sCopy)
	return history
}

func calc(filename string) int {
	s := readInput(filename)
	toVisit := make([]*currentSituation, 1)
	visited := make([]*currentSituation, 0)
	bestResult := -1
	toVisit[0] = s
	for len(toVisit) > 0 {
		s = toVisit[0]
		toVisit = toVisit[1:]
		visited = appendIfNotVisited(visited, s)
		moveIns, moveOuts := s.possibleMovements()
		for _, moveIn := range moveIns {
			sCopy := s.deepCopy()
			addScore := sCopy.moveIn(moveIn.from, moveIn.to)
			sCopy.score += addScore
			if sCopy.signature == targetSituation {
				if bestResult < 0 {
					bestResult = sCopy.score
				}
				bestResult = aoc.Min(bestResult, sCopy.score)
			}
			toVisit = scheduleVisit(toVisit, sCopy, visited)
		}
		for _, moveOut := range moveOuts {
			sCopy := s.deepCopy()
			addScore := sCopy.moveOut(moveOut.from, moveOut.to)
			sCopy.score += addScore
			toVisit = scheduleVisit(toVisit, sCopy, visited)
		}
	}
	return s.score
}

func main() {
	f, err := os.Create("pprof_cpu.prof")
	if err != nil {
		panic(err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			panic(err)
		}
	}(f)
	if err := pprof.StartCPUProfile(f); err != nil {
		panic(err)
	}
	defer pprof.StopCPUProfile()

	fmt.Println(calc("test.txt"))
}
