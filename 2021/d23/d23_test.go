package main

import "testing"

func TestMoveOut(t *testing.T) {
	var score int

	situation := getInitial()

	// legal move
	from := stashedPosition{
		corridor: 2,
		slot:     1,
	}
	to := hallwayPosition{location: 3}
	canDo := situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if !canDo || score != 40 {
		t.Fail()
	}

	// source must be amphipod
	from = stashedPosition{
		corridor: 2,
		slot:     1,
	}
	to = hallwayPosition{location: 5}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}

	// destination must not be amphipod
	from = stashedPosition{
		corridor: 1,
		slot:     1,
	}
	to = hallwayPosition{location: 3}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}

	// jump over amphipod in hallway must fail
	from = stashedPosition{
		corridor: 2,
		slot:     0,
	}
	to = hallwayPosition{location: 1}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}

	// block entrance must fail
	from = stashedPosition{
		corridor: 2,
		slot:     1,
	}
	to = hallwayPosition{location: 4}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}

	// legal move
	from = stashedPosition{
		corridor: 2,
		slot:     0,
	}
	to = hallwayPosition{location: 5}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		score = situation.moveOut(from, to)
	}
	if !canDo || score != 300 {
		t.Fail()
	}

	situation = getInitial()

	// block entrance must fail
	from = stashedPosition{
		corridor: 2,
		slot:     1,
	}
	to = hallwayPosition{location: 2}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		_ = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}

	// jump over amphipod above must fail
	from = stashedPosition{
		corridor: 2,
		slot:     0,
	}
	to = hallwayPosition{location: 3}
	canDo = situation.moveOutIsValid(from, to)
	if canDo {
		_ = situation.moveOut(from, to)
	}
	if canDo {
		t.Fail()
	}
}

func TestMoveIn(t *testing.T) {
	var score int

	situation := getArbitrary()

	// Can't move into wrong corridor
	from := hallwayPosition{location: 5}
	to := stashedPosition{
		corridor: 1,
		slot:     0,
	}
	canDo := situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if canDo {
		t.Fail()
	}

	// Can't move into full corridor
	from = hallwayPosition{location: 0}
	to = stashedPosition{
		corridor: 0,
		slot:     0,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if canDo {
		t.Fail()
	}

	// Can't jump over other amphipod in hallway
	from = hallwayPosition{location: 9}
	to = stashedPosition{
		corridor: 2,
		slot:     0,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if canDo {
		t.Fail()
	}

	// Can't jump over other amphipod in corridor
	from = hallwayPosition{location: 7}
	to = stashedPosition{
		corridor: 3,
		slot:     0,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if canDo {
		t.Fail()
	}

	// Can't go to above slot if below isn't occupied
	from = hallwayPosition{location: 5}
	to = stashedPosition{
		corridor: 2,
		slot:     1,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if canDo {
		t.Fail()
	}

	// Legal move: above slot
	from = hallwayPosition{location: 7}
	to = stashedPosition{
		corridor: 3,
		slot:     1,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if !canDo || score != 2000 {
		t.Fail()
	}

	// Legal move: below slot
	from = hallwayPosition{location: 5}
	to = stashedPosition{
		corridor: 2,
		slot:     0,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if !canDo || score != 300 {
		t.Fail()
	}

	// legal move: hallway cleared
	from = hallwayPosition{location: 9}
	to = stashedPosition{
		corridor: 2,
		slot:     1,
	}
	canDo = situation.moveInIsValid(from, to)
	if canDo {
		score = situation.moveIn(from, to)
	}
	if !canDo || score != 400 {
		t.Fail()
	}
}

func TestPossibleMovements(t *testing.T) {
	moveIns, moveOuts := getInitial().possibleMovements()
	if len(moveIns) != 0 || len(moveOuts) != 28 {
		t.Fail()
	}
	moveIns, moveOuts = getArbitrary().possibleMovements()
	if len(moveOuts) != 2 || len(moveIns) != 2 {
		t.Fail()
	}
}

func getInitial() *currentSituation {
	initial := &currentSituation{
		stashedAmphipods: map[stashedPosition]string{
			stashedPosition{
				corridor: 0,
				slot:     0,
			}: "A",
			stashedPosition{
				corridor: 0,
				slot:     1,
			}: "B",
			stashedPosition{
				corridor: 1,
				slot:     0,
			}: "D",
			stashedPosition{
				corridor: 1,
				slot:     1,
			}: "C",
			stashedPosition{
				corridor: 2,
				slot:     0,
			}: "C",
			stashedPosition{
				corridor: 2,
				slot:     1,
			}: "B",
			stashedPosition{
				corridor: 3,
				slot:     0,
			}: "A",
			stashedPosition{
				corridor: 3,
				slot:     1,
			}: "D",
		},
		hallwayAmphipods: map[hallwayPosition]string{},
	}
	initial.signature = genSignature(initial.stashedAmphipods, initial.hallwayAmphipods)
	return initial
}

func getArbitrary() *currentSituation {
	arbitrary := &currentSituation{
		stashedAmphipods: map[stashedPosition]string{
			stashedPosition{
				corridor: 0,
				slot:     0,
			}: "A",
			stashedPosition{
				corridor: 0,
				slot:     1,
			}: "B",
			stashedPosition{
				corridor: 3,
				slot:     0,
			}: "D",
		},
		hallwayAmphipods: map[hallwayPosition]string{
			hallwayPosition{location: 0}: "A",
			hallwayPosition{location: 5}: "C",
			hallwayPosition{location: 7}: "D",
			hallwayPosition{location: 9}: "C",
		},
	}
	arbitrary.signature = genSignature(arbitrary.stashedAmphipods, arbitrary.hallwayAmphipods)
	return arbitrary
}
