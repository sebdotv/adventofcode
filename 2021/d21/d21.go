package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strconv"
    "strings"
)

var mulFactor = map[int]int64{
    3: 1,
    4: 3,
    5: 6,
    6: 7,
    7: 6,
    8: 3,
    9: 1,
}

type worldState struct {
    multiplicity int64
    s1           int
    s2           int
    p1           int
    p2           int
    turn         int
}

type die interface {
    cast() int
    nbCasts() int
}

type hundie struct {
    curVal int
    count  int
}

func (h *hundie) cast() int {
    res := 0
    h.count += 3
    for i := 0; i < 3; i++ {
        h.curVal++
        if h.curVal > 100 {
            h.curVal = 1
        }
        res += h.curVal
    }
    return res
}

func (h *hundie) nbCasts() int {
    return h.count
}

func readInput(filename string) (int, int) {
    lines := aoc.ReadInputLines(filename)
    l1 := strings.Split(lines[0], " ")
    l2 := strings.Split(lines[1], " ")
    p1, _ := strconv.ParseInt(l1[len(l1)-1], 10, 64)
    p2, _ := strconv.ParseInt(l2[len(l2)-1], 10, 64)
    return int(p1) - 1, int(p2) - 1
}

func calc(filename string) int {
    _, score, nbCasts := play(&hundie{}, 1000, filename)
    return score * nbCasts
}

func play(d die, maxScore int, filename string) (int, int, int) {
    p1, p2 := readInput(filename)
    s1, s2 := 0, 0
    for {
        p1 = (p1 + d.cast()) % 10
        s1 += p1 + 1
        if s1 >= maxScore {
            return 1, s2, d.nbCasts()
        }
        p2 = (p2 + d.cast()) % 10
        s2 += p2 + 1
        if s2 >= maxScore {
            return 2, s1, d.nbCasts()
        }
    }
}

func calc2(filename string) int64 {
    initP1, initP2 := readInput(filename)
    initW := worldState{
        multiplicity: 1,
        s1:           0,
        s2:           0,
        p1:           initP1,
        p2:           initP2,
        turn:         1,
    }
    runningGames := []worldState{initW}
    var wonBy1, wonBy2 int64
    for len(runningGames) > 0 {
        game := runningGames[0]
        runningGames = runningGames[1:]
        for cast, mul := range mulFactor {
            var p1, p2, s1, s2 int
            if game.turn == 1 {
                p1 = (game.p1 + cast) % 10
                p2 = game.p2
                s1 = game.s1 + p1 + 1
                s2 = game.s2
            } else {
                p1 = game.p1
                p2 = (game.p2 + cast) % 10
                s1 = game.s1
                s2 = game.s2 + p2 + 1
            }
            newMultiplicity := mul * game.multiplicity
            if s1 >= 21 || s2 >= 21 {
                if s1 >= 21 {
                    wonBy1 += newMultiplicity
                } else {
                    wonBy2 += newMultiplicity
                }
            } else {
                runningGames = append(runningGames, worldState{
                    multiplicity: newMultiplicity,
                    s1:           s1,
                    s2:           s2,
                    p1:           p1,
                    p2:           p2,
                    turn:         3 - game.turn,
                })
            }
        }
    }
    if wonBy1 > wonBy2 {
        return wonBy1
    }
    return wonBy2
}

func main() {
    aoc.CheckEquals(739785, calc("test.txt"))
    aoc.CheckEquals(444356092776315, int(calc2("test.txt")))
    fmt.Println(calc("input.txt"))
    fmt.Println(calc2("input.txt"))
}
