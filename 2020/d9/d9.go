package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const exp1 = 127
const exp2 = 62

func check(preamble []int, value int) bool {
	for i, v := range preamble {
		if aoc.Contains(preamble[i+1:], value-v) {
			return true
		}
	}
	return false
}

func calc(xmas []int, preambleSize int) int {
	for i := preambleSize; i < len(xmas); i++ {
		if !check(xmas[i-preambleSize:i], xmas[i]) {
			return xmas[i]
		}
	}
	panic("everything matched")
}

func calc2(xmas []int, expectedResult int) int {
	var smallest, biggest int
	for i, sequenceStart := range xmas {
		result := sequenceStart
		smallest = sequenceStart
		biggest = sequenceStart
		for _, sequenceEnd := range xmas[i+1:] {
			result += sequenceEnd
			smallest = aoc.Min(smallest, sequenceEnd)
			biggest = aoc.Max(biggest, sequenceEnd)
			if result == expectedResult {
				return smallest + biggest
			}
			if result > expectedResult {
				break
			}
		}
	}
	panic("nothing found")
}

func main() {
	testData := aoc.ReadInputIntegers("test.txt")
	t1 := calc(testData, 5)
	t2 := calc2(testData, t1)
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	aoc.CheckEqualsMsg(exp2, t2, "test2")
	data := aoc.ReadInputIntegers("input.txt")
	r1 := calc(data, 25)
	r2 := calc2(data, r1)
	fmt.Printf("result1: %d\n", r1)
	fmt.Printf("result2: %d\n", r2)
}
