exp1 = 7
exp2 = 336


def read_input(filename):
    with open(filename, 'r') as of:
        lines = list(map(str.strip, of.readlines()))
        forest = {(line, col): ['.', '#'].index(lines[line][col]) for line in range(len(lines)) for col in
                  range(len(lines[line]))}
        return forest, len(lines), int(len(forest) / len(lines))


def calc(filename, mx=3, my=1):
    forest, height, width = read_input(filename)
    x, y = 0, 0
    trees = 0
    while y < height:
        trees += forest[(y, x)]
        x += mx
        x %= width
        y += my
    return trees


def calc2(filename):
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    result = 1
    for s in slopes:
        result *= calc(filename, *s)
    return result


if __name__ == '__main__':
    t1 = calc("test.txt")
    t2 = calc2("test.txt")
    if t1 != exp1:
        print(f"test1: expected {exp1}, got {t1}")
    if t2 != exp2:
        print(f"test2: expected {exp2}, got {t2}")
    print(f"result1: {calc('input.txt')}")
    print(f"result2: {calc2('input.txt')}")
