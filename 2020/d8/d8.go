package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const (
	EXP1 = 5
	EXP2 = 8
	NOP  = "nop"
	ACC  = "acc"
	JMP  = "jmp"
)

type instruction struct {
	op  string
	val int
}

func readInput(filename string) []*instruction {
	instructions := aoc.ReadInputLines(filename)
	result := make([]*instruction, len(instructions))
	for i, instr := range instructions {
		instrSplit := strings.Split(instr, " ")
		value, _ := strconv.Atoi(instrSplit[1])
		result[i] = &instruction{
			op:  instrSplit[0],
			val: value,
		}
	}
	return result
}

func execInstruction(instr *instruction) (int, int) {
	switch instr.op {
	case NOP:
		return 0, 0
	case ACC:
		return instr.val, 0
	case JMP:
		return 0, instr.val - 1
	}
	panic(fmt.Errorf("unknown op %v", instr.op))
}

func calc(instrs []*instruction) (int, int) {
	acc := 0
	linesRan := make([]int, 0)
	curLine := 0
	for !aoc.Contains(linesRan, curLine) && curLine < len(instrs) {
		linesRan = append(linesRan, curLine)
		accDelta, jmpLine := execInstruction(instrs[curLine])
		curLine += 1 + jmpLine
		acc += accDelta
	}
	return acc, curLine
}

func calc2(instrs []*instruction) int {
	for _, instr := range instrs {
		switch {
		case instr.op == NOP:
			instr.op = JMP
			acc, curLine := calc(instrs)
			if curLine == len(instrs)-1 {
				return acc
			}
			instr.op = NOP
		case instr.op == JMP:
			instr.op = NOP
			acc, curLine := calc(instrs)
			if curLine == len(instrs) {
				return acc
			}
			instr.op = JMP
		}
	}
	panic(fmt.Errorf("never reached the end"))
}

func main() {
	testInstrs := readInput("test.txt")
	t1, _ := calc(testInstrs)
	t2 := calc2(testInstrs)
	aoc.CheckEquals(EXP1, t1)
	aoc.CheckEquals(EXP2, t2)
	instrs := readInput("input.txt")
	r1, _ := calc(instrs)
	r2 := calc2(instrs)
	fmt.Printf("Result 1: %d\n", r1)
	fmt.Printf("Result 2: %d\n", r2)
}
