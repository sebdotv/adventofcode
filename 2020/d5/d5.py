def read_input(filename):
    with open(filename, "r") as of:
        return map(str.strip, of.readlines())


def seat_number(seat_str):
    return int(seat_str.replace("B", "1").replace("F", "0").replace("L", "0").replace("R", "1"), 2)


def calc(filename):
    seat_strs = read_input(filename)
    return max(map(seat_number, seat_strs))


def calc2(filename):
    seat_strs = read_input(filename)
    allocated_seats = set(map(seat_number, seat_strs))
    for c in range(1+min(allocated_seats), max(allocated_seats)):
        if c not in allocated_seats and {c-1, c+1}.issubset(allocated_seats):
            return c


def test():
    tests = {
        "FBFBBFFRLR": 357,
        "BFFFBBFRRR": 567,
        "FFFBBBFRRR": 119,
        "BBFFBBFRLL": 820,
        "FFFFFFFLLL": 0,
        "BBBBBBBRRR": 1023,
    }
    assert all(set(seat_number(k) == v for k, v in tests.items()))


if __name__ == '__main__':
    test()
    print(f"answer 1: {calc('input.txt')}")
    print(f"answer 2: {calc2('input.txt')}")
