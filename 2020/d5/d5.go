package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"sort"
	"strconv"
	"strings"
)

func seatNumber(seatStr string) int {
	binarySeat := strings.ReplaceAll(seatStr, "B", "1")
	binarySeat = strings.ReplaceAll(binarySeat, "F", "0")
	binarySeat = strings.ReplaceAll(binarySeat, "L", "0")
	binarySeat = strings.ReplaceAll(binarySeat, "R", "1")
	result, _ := strconv.ParseInt(binarySeat, 2, 0)
	return int(result)
}

func readSeatNumbers(filename string) (int, int) {
	seatStrs := aoc.ReadInputLines(filename)
	seatNbs := make([]int, len(seatStrs))
	minimum := 1024
	maximum := 0
	for i, s := range seatStrs {
		seatNbs[i] = seatNumber(s)
		if seatNbs[i] > maximum {
			maximum = seatNbs[i]
		}
		if seatNbs[i] < minimum {
			minimum = seatNbs[i]
		}
	}
	sort.Ints(seatNbs)
	missing := minimum
	for _, s := range seatNbs {
		if missing != s {
			break
		}
		missing = s + 1
	}
	return maximum, missing
}


func main() {
	r1, r2 := readSeatNumbers("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
